﻿// Class for testing purpose
// It's a good idea to create ISerializable interface with OnSerialize and OnDeserialize methods and inherit every class that we want store from it.
// Because current solution wouldn't be enough for some cases such as GameObject serialization

using UnityEngine;

[System.Serializable]
public class TestClass /* : ISerializable*/
{
    public string name;
    public float num;
    [SerializeField]
    private int privateNum;

    public TestClass(string name, float num)
    {
        this.name = name;
        this.num = num;
        privateNum = UnityEngine.Random.Range(0, 10);
    }

    public override string ToString()
    {
        return name + " = " + num + " = " + privateNum;
    }
}
