﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StorageMediumType
{
    PlayerPrefs,
    Memory,
    File
}

