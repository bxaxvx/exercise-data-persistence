﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryMedium : IStorageMedium
{
    private Dictionary<string, DataContainer> dataDictionary = new Dictionary<string, DataContainer>();

    public MemoryMedium(StorageOptionsInfo config)
    {
    }

    public T ReadData<T>(string key, ref bool isSuccess)
    {
        if (dataDictionary.ContainsKey(key))
        {
            if (dataDictionary[key].type == typeof(T))
            {
                isSuccess = true;
                return (T)dataDictionary[key].data;
            }
        }
        isSuccess = false;
        return default(T);
    }

    public void WriteData<T>(string key, T value, ref bool isSuccess)
    {
        isSuccess = true;
        if (dataDictionary.ContainsKey(key))
        {
            dataDictionary[key].data = value;
            dataDictionary[key].type = typeof(T);
        }
        else
            dataDictionary.Add(key, new DataContainer(value, typeof(T)));
    }
}
