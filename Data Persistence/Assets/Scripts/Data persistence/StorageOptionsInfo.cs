﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Scriptable object is a great solution for storing config files

[CreateAssetMenu(fileName = "Storage options", menuName = "Data/Storage options")]
public class StorageOptionsInfo : ScriptableObject
{
    public StorageMediumType storageMediumType;
    public string[] options;
}
