﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class that we use to interact with storage mediums. I inherited MonoBehaviour to be able easily add a reference to the storage config file.   

public class DataManager : MonoBehaviour
{
    [SerializeField]
    private StorageOptionsInfo storageConfig;
    private IStorageMedium storageMedium;

    public IStorageMedium StorageMedium
    {
        get
        {
            if (storageMedium != null)
                return storageMedium;
            switch (storageConfig.storageMediumType)
            {
                case StorageMediumType.File:
                    storageMedium = new FileMedium(storageConfig);break;
                case StorageMediumType.Memory:
                    storageMedium = new MemoryMedium(storageConfig); break;
                case StorageMediumType.PlayerPrefs:
                    storageMedium = new PlayerPrefsMedium(storageConfig); break;
            }
            return storageMedium;
        }
    }
}
