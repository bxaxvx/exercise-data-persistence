﻿using System;

// Used for storing data

[Serializable]
public class DataContainer
{
    public Type type;
    public System.Object data;

    public DataContainer(object value, Type type)
    {
        data = value;
        this.type = type;
    }
}