﻿using System;
using UnityEngine;

// PlayerPrefs storage medium class. I decided to store complex data type with JSON serialization inside string PlayerPrefs to make solution more generic
// We can't serializae primitive types such as int, float to JSON so it saved to PlayerPrefs simply as a string

public class PlayerPrefsMedium : IStorageMedium
{
    public PlayerPrefsMedium(StorageOptionsInfo config)
    {
    }

    public T ReadData<T>(string key, ref bool isSuccess)
    {
        if (!PlayerPrefs.HasKey(key))
        {
            isSuccess = false;
            return default(T);
        }
        try
        {
            isSuccess = true;
            T t;
            if (IsSimpleType(typeof(T)))
                t = (T)Convert.ChangeType(PlayerPrefs.GetString(key), typeof(T));
            else
                t = JsonUtility.FromJson<T>(PlayerPrefs.GetString(key));
            return t;
        }
        catch (System.Exception e)
        {
            isSuccess = false;
            return default(T);
        }
    }

    public void WriteData<T>(string key, T value, ref bool isSuccess)
    {
        if (IsSimpleType(typeof(T)))
            PlayerPrefs.SetString(key, value.ToString());
        else
            PlayerPrefs.SetString(key, JsonUtility.ToJson(value));
        isSuccess = true;
    }

    private bool IsSimpleType(Type type)
    {
        return type.IsPrimitive || type.Equals(typeof(string));
    }
}
