﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Interface from which every Storage Medium inherits
// It might be a good idea to return errorMessage as well

public interface IStorageMedium
{
    T ReadData<T>(string key, ref bool isSuccess/*, ref string errorMessage*/);
    void WriteData<T>(string key, T value, ref bool isSuccess/*, ref string errorMessage*/);
}
