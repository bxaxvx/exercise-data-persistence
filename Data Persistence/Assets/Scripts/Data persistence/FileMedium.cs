﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

// In this storage medium I use custom options for file name and for caching in memory

public class FileMedium : IStorageMedium
{
    private const string DEFAULT_FILE_NAME = "data.dat";
    private Dictionary<string, DataContainer> dataCache;

    private string fileName;
    // If cacheInMemory set to true then we store our data in memory after reading or writing to file.
    // It's handy to reduce access time to data, because reading from file is slow.
    // But it is a bad idea to use this option if we have huge amount of data.
    private bool cacheInMemory;

    private string FilePath
    {
        get
        {
            return Application.persistentDataPath + (string.IsNullOrEmpty(fileName) ? DEFAULT_FILE_NAME : fileName);
        }
    }

    // Basic implementation for options parsing

    private void ParseOptions(string[] options)
    {
        foreach (var row in options)
        {
            string[] elements = row.Split(' ');
            if (elements.Length == 0)
                continue;
            switch (elements[0].ToLower())
            {
                case "filename": if (elements.Length > 1) fileName = elements[1]; break;
                case "cacheinmemory": cacheInMemory = true; break;
            }
        }
    }

    public FileMedium(StorageOptionsInfo config)
    {
        ParseOptions(config.options);
    }

    public T ReadData<T>(string key, ref bool isSuccess)
    {
        Dictionary<string, DataContainer> data = cacheInMemory && (dataCache != null) ? dataCache : LoadDataFromFile(FilePath);
        if (cacheInMemory)
            dataCache = data;
        if (data.ContainsKey(key))
        {
            if (data[key].type == typeof(T))
            {
                isSuccess = true;
                return (T)data[key].data;
            }
        }
        isSuccess = false;
        return default(T);
    }

    public void WriteData<T>(string key, T value, ref bool isSuccess)
    {
        Dictionary<string, DataContainer> data = cacheInMemory && (dataCache != null) ? dataCache : LoadDataFromFile(FilePath);
        if (cacheInMemory)
            dataCache = data;
        if (data.ContainsKey(key))
        {
            data[key].data = value;
            data[key].type = typeof(T);
        }
        else
            data.Add(key, new DataContainer(value, typeof(T)));
        WriteDataToFile(FilePath, data);
        isSuccess = true;
    }

    private void WriteDataToFile(string filePath, Dictionary<string, DataContainer> data)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
    }

    private Dictionary<string, DataContainer> LoadDataFromFile(string filePath)
    {
        if (File.Exists(filePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(filePath, FileMode.Open);
            Dictionary<string, DataContainer> data = (Dictionary<string, DataContainer>)bf.Deserialize(file);
            file.Close();
            return data;
        }
        else
            return new Dictionary<string, DataContainer>();
    }
}
