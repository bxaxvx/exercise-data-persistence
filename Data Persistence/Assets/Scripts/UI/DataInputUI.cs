﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataInputUI : MonoBehaviour
{
    [SerializeField]
    private Toggle activatedToggle;
    [SerializeField]
    private InputField inputField;

    public bool IsActivated
    {
        get
        {
            return activatedToggle.isOn;
        }
    }

    public string Value
    {
        get
        {
            return inputField.text;
        }
        set
        {
            inputField.text = value;
        }
    }
}
