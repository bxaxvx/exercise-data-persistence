﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoolInputUI : MonoBehaviour
{
    [SerializeField]
    private Toggle activatedToggle;
    [SerializeField]
    private Toggle inputToggle;

    public bool IsActivated
    {
        get
        {
            return activatedToggle.isOn;
        }
    }

    public bool Value
    {
        get
        {
            return inputToggle.isOn;
        }
        set
        {
            inputToggle.isOn = value;
        }
    }
}
