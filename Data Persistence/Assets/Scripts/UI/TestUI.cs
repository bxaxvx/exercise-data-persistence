﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Data view class for testing purposes

public class TestUI : MonoBehaviour
{
    [SerializeField]
    private DataManager dataManager;

    [SerializeField]
    private Text messageText;

    [SerializeField]
    private DataInputUI floatInputField;
    [SerializeField]
    private DataInputUI intInputField;
    [SerializeField]
    private DataInputUI stringInputField;
    [SerializeField]
    private BoolInputUI boolInputToggle;
    [SerializeField]
    private InputField inputKeyField;

    [SerializeField]
    private DataInputUI floatOutputField;
    [SerializeField]
    private DataInputUI intOutputField;
    [SerializeField]
    private DataInputUI stringOutputField;
    [SerializeField]
    private BoolInputUI boolOutputToggle;
    [SerializeField]
    private InputField outputKeyField;

    #region Mono behaviour

    private void Awake()
    {
        // Test if Custom classes are working
        bool isSuccess = false;
        dataManager.StorageMedium.WriteData("testClass", new TestClass("testclass", 55), ref isSuccess);
        Debug.Log(dataManager.StorageMedium.ReadData<TestClass>("testClass", ref isSuccess).ToString());
        if (isSuccess)
            Debug.Log("Classes saving/loading is working.");
    }

    #endregion

    public void LoadData()
    {
        if (string.IsNullOrEmpty(outputKeyField.text))
            messageText.text = "Key is empty.";
        else
        {
            bool isSuccess = false;
            ClearOutput();
            if (floatOutputField.IsActivated)
                floatOutputField.Value = dataManager.StorageMedium.ReadData<float>(outputKeyField.text,  ref isSuccess).ToString();
            else if (intOutputField.IsActivated)
                intOutputField.Value = dataManager.StorageMedium.ReadData<int>(outputKeyField.text, ref isSuccess).ToString();
            else if (stringOutputField.IsActivated)
                stringOutputField.Value = dataManager.StorageMedium.ReadData<string>(outputKeyField.text, ref isSuccess);
            else if (boolOutputToggle.IsActivated)
                boolOutputToggle.Value = dataManager.StorageMedium.ReadData<bool>(outputKeyField.text, ref isSuccess);
            if (isSuccess)
                messageText.text = "Loaded data";
            else
                messageText.text = "Failed to load data";
        }
    }

    private void ClearOutput()
    {
        floatOutputField.Value = "";
        intOutputField.Value = "";
        stringOutputField.Value = "";
        boolOutputToggle.Value = false;
    }

    public void SaveData()
    {
        if (string.IsNullOrEmpty(inputKeyField.text))
            messageText.text = "Key is empty.";
        else
        {
            bool isSuccess = false;
            if (floatInputField.IsActivated)
                dataManager.StorageMedium.WriteData(inputKeyField.text, float.Parse(floatInputField.Value), ref isSuccess);
            else if (intInputField.IsActivated)
                dataManager.StorageMedium.WriteData(inputKeyField.text, int.Parse(intInputField.Value), ref isSuccess);
            else if (stringInputField.IsActivated)
                dataManager.StorageMedium.WriteData(inputKeyField.text, stringInputField.Value, ref isSuccess);
            else if (boolInputToggle.IsActivated)
                dataManager.StorageMedium.WriteData(inputKeyField.text, boolInputToggle.Value, ref isSuccess);
            if (isSuccess)
                messageText.text = "Saved data";
            else
                messageText.text = "Failed to save data";
        }
    }
}
