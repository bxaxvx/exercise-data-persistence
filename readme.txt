Hello

To test exercise drag desired config file from "Storage configs" folder to "DataManager" game object in the Main scene.
Run the app, select desired type from Input/Output, enter "Data key" and click on the Read/Write button.

I added support for storing arbitrary data types (structs or classes).

link to the repository - https://bitbucket.org/bxaxvx/exercise-data-persistence/src

I had fun making this exercise, hope you will have fun reviewing it as well.
If you have any questions or feedback feel free to contact me at bxaxvx@gmail.com

Andriy Bychkovskyi